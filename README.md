# Runaway hands-on session 
---

Runaway is a command line tool that aims to help you run your scripts on remote hosts more easily. For instance, imagine you have this cool experiment on your laptop that you would love to deploy as an experimental campaign on this super large cluster of your lab. Running the experiment on your laptop is pretty simple:

```bash
$ ./coolscript.py --my-param=1 --my-flag
```

Question is, how to scale this experiment to your cluster? With _runaway_, running your script can be as simple as:
```bash
$ runaway exec supercluster coolscript.py -- --my-param=1 --my-flag
```

Performing hyper parameter tuning can be reduced to:
```bash
$ runaway sched supercluster coolscript.py scheduler.py
```

And performing your experimental campaign can be reduced to 
```bash
$ runaway batch supercluster coolscript.py -P parameters.txt
```

During this hands on session, we will discover the main use-cases of runaway. The explanations are presented in the following notebooks:
+ [1_install.ipynb](1_install.ipynb): Install the binary and a few of the available remote profiles.
+ [2_exec.ipynb](2_exec.ipynb): Execute an experiment on a remote host
+ [3_batch.ipynb](3_exec.ipynb): Execute a batch of experiments on a remote host
+ [4_sched.ipynb](4_sched.ipynb): Use a scheduler to adapt to complex workflows
+ [5_advanced.ipynb](5_advanced.ipynb): Advanced tricks and optimization