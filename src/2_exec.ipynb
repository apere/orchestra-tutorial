{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Executing code on remote host with `runaway exec`\n",
    "---------------------------------------------------\n",
    "\n",
    "We should now be ready to start using runaway. The first thing we will do is execute a single experiment on a remote platform using runaway.\n",
    "\n",
    "## A facebook experiment\n",
    "\n",
    "For this workshop, you will be working on the experiment of the facebook paper [mixup: Beyond Empirical Risk Minimization](https://github.com/facebookresearch/mixup-cifar10). To ease things I forked it, and made a few changes to the original repository, but you could run it from the original repository as well.\n",
    "\n",
    "Let's clone it:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "git clone https://gitlab.inria.fr/apere/mixup-cifar10.git\n",
    "cd mixup-cifar10\n",
    "chmod +x train.py\n",
    "ls -lh"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The experiment is pretty simple:\n",
    "+ The experiment is contained in the `train.py` script\n",
    "+ It loads data from the `./data` folder (data are downloaded at first execution)\n",
    "+ Performs the experiment\n",
    "+ Stores the results in the `./results` and `./checkpoints`\n",
    "\n",
    "Let's run it once in local to download the data:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "./train.py --dry-run --epoch=1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It downloads the data and perform a first training loop (the `--dry-run` option performs a single batch to make things faster). We can see that the execution created folders (`checkpoint` and `results`), which were both filled with data:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "echo \"Experiment folder:\"\n",
    "ls -lh\n",
    "echo -e \"\\nCheckpoint folder:\"\n",
    "ls -lh checkpoint\n",
    "echo -e \"\\nResults folder:\"\n",
    "ls -lh results"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's remove those results for now:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "rm -rf checkpoint results"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Basic use of the `exec` subcommand.\n",
    "\n",
    "The first thing you can do with runaway, is to run a single execution of your local code, using the `runaway exec` subcommand. To do that on the localhost profile for instance, you can proceed with the following command:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cd mixup-cifar10"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "runaway exec localhost train.py -- --dry-run --epoch=1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<br>\n",
    "\n",
    "The first thing to notice is that runaway copies the stdout and stderr of the remote command, but also emits a few messages on the stderr. Those messages are always prefixed with `runaway:` . You can turn off those messages by using the `--silent` option, which would give the same output as if it was run on your local computer.\n",
    "\n",
    "If you take a look at the runaway messages, you can get a glimpse of what happened in the background, but let us make a quick summary:\n",
    "\n",
    "![](../img/exec_process.png)\n",
    "\n",
    "According to that, we can see that the results folder are filled with the output data: \n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "echo \"Experiment folder:\"\n",
    "ls -lh\n",
    "echo -e \"\\nCheckpoint folder:\"\n",
    "ls -lh checkpoint\n",
    "echo -e \"\\nResults folder:\"\n",
    "ls -lh results"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Okay, so that was not super helpful since the code was run on our local machine. Let's try to run the same experiment on the `flowers-gpu` profile:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "runaway exec flowers-gpu train.py -- --dry-run --epoch=1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If your python environment was setup correctly on the remote, you should see the same results that occurred earlier on `localhost`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Reducing transfer\n",
    "\n",
    "If you think about the process that runs in the background as runaway performs the execution, you can see an immediate pitfall concerning data transfer. Indeed, when runaway sends the data, all of the current directory is sent to the remote, which includes:\n",
    "+ All the `git` repository\n",
    "+ Both the compressed `data/cifar-10-python.tar.gz` an the uncompressed data `data/cifar-10-batches-py/*`\n",
    "+ Every python cache files `*.pyc` you may find in the repo.\n",
    "\n",
    "Moreover, the same thing happens again when the data are fetched. Hopefully, runaway has a way to handle that. You can write `.sendignore` and `.fetchignore` files, with the same globs you would use in a git repository, to include or exclude files from the sending and fetching phase.\n",
    "\n",
    "Let's write some for our data:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Changes the sendignore\n",
    "cat << EOF >> .sendignore\n",
    "data/cifar-10-python.tar.gz\n",
    "results/*\n",
    "batch/*\n",
    "checkpoint/*\n",
    "*__pycache__/*\n",
    ".git/*\n",
    "EOF"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Changes the sendignore\n",
    "cat << EOF >> .fetchignore\n",
    "LICENSE*\n",
    "models/*\n",
    "data/*\n",
    "*.pyc\n",
    "README.MD\n",
    "*.py\n",
    "EOF"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A few options and tricks are available, all explained in the [Advanced](5_advanced.html) notebook. But for now, you can continue with the [Batch](3_batch.html) notebook."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Bash",
   "language": "bash",
   "name": "bash"
  },
  "language_info": {
   "codemirror_mode": "shell",
   "file_extension": ".sh",
   "mimetype": "text/x-sh",
   "name": "bash"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
